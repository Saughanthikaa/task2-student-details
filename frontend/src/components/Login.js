
import React, { useState } from 'react';
import axios from 'axios';
import "primereact/resources/themes/lara-light-indigo/theme.css"; 
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { useNavigate } from 'react-router-dom';


        
function Login() {
    
    const [userid, setUserid] = useState('');
    const [password, setPassword] = useState('');
   // const history = useHistory();
    const navigate=useNavigate()
    const handleRegister=()=>{
        navigate('/LoginPage')
    }
    const handleSubmit = (event) => {
        event.preventDefault();
        axios.post(`http://localhost:5001/newlogin/${userid}`, {
            userid: userid,
            password: password
        })
        .then(response => {
            console.log(response)
            if (response.data === "success") {
                navigate('/NavPage', { state: { userid } });
            } else {
                alert('Invalid login credentials.');
            }
        })
        .catch(error => {
            console.log(error);
        });
    };

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Student id:
                    <br />
                    <InputText value={userid} onChange={(e) => setUserid(e.target.value)} />
                    <br />
                </label>
                <label>
                    <br />
                    Password:
                    <br />
                    <InputText type='password' value={password} onChange={(e) => setPassword(e.target.value)} />
                    <br />
                    <br />
                </label>
                <br />
                <Button label="Login" type="submit" />
                <br /><br />
                
            </form>
            <Button label="Register"onClick={handleRegister} />
        </div>
    );
}

export default Login;