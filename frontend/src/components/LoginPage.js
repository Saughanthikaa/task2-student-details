import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import "primereact/resources/themes/lara-light-indigo/theme.css"; 
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import Login from './Login';
import { Card } from 'primereact/card';
import Add_student from './Add_student';
        
        
function LoginPage() {
  const [userid, setUserid] = useState('');
  const [password, setPassword] = useState('');
  const [cpassword, setCPassword] = useState('');
  const [registerStatus, setregisterStatus] = useState(null);
  const navigate = useNavigate()

  const handleSubmit = (e) => {
    e.preventDefault();
    if (password !== cpassword) {
      setregisterStatus("Passwords do not match. Please try again.");
      setPassword('');
      setCPassword('');
    } else {
      let formData = {
        id: userid,
        password: password,
        cpassword: cpassword,
      }
      axios.post(`http://localhost:5001/login`, formData)
      .then(response => {
        console.log(response)
        if (response.data === "success") {
          console.log("yesss")
            navigate('/Login');
        } else {
            alert('Id already exits try Login!!.');
        }
    })
    }
  }

  return (
    <div className="card">
      <Card>
        <form onSubmit={handleSubmit}>
          <div>
            <label>
              UserId:
              <br></br>
              <InputText value={userid} onChange={(e) => setUserid(e.target.value)} />
            </label>
          </div>
          <div>
            <label>
              Password:
              <br></br>
              <InputText type='password' value={password} onChange={(e) => setPassword(e.target.value)} />
            </label>
          </div>
          <br></br>
          <div>
            <label>
              Confirm Password:
              <br></br>
              <InputText type='password' value={cpassword} onChange={(e) => setCPassword(e.target.value)} />
            </label>
          </div>
          <br></br>
          <Button label="Submit" />
          {registerStatus && <p>{registerStatus}</p>}
        </form>
      </Card>
    </div>
  );
}


export default LoginPage;
