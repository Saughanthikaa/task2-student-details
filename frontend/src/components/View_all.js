import React, { useState } from "react";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";

function View_all() {
  const [userId, setUserId] = useState("");
  const [userData, setUserData] = useState(null);

  const handleClick = () => {
    if (userId) {
      fetch(`http://localhost:5001/view_all/${userId}`)
        .then((response) => response.json())
        .then((data) => setUserData(data))
        .catch((error) => console.error(error));
    }
  };

  return (
    <div>
      <div>
        <label>
          Student id:
          <br />
          <InputText value={userId} onChange={(e) => setUserId(e.target.value)} />
          <br />
          <Button label="View" onClick={handleClick} />
          <br />
        </label>
      </div>

      {/* Conditional rendering block */}
      {userData && (
        <div>
          <h3>Student Details:</h3>
          <p>Name: {userData.name}</p>
          <p>Email: {userData.emaill}</p>
          <p>Phone: {userData.phone}</p>
          <p>Attendace:{userData.attendance}</p>
          {/* Add more fields here */}
        </div>
      )}
    </div>
  );
}

export default View_all;
